#pragma once

#include <cstdint>
#include <iostream>
#include <sstream>
#include <string>

#define N 7
// Nmax=7
#if N > 7
#error "bees"
#endif
enum symmetries { C4_1, C4_4 };

#define SYM C4_1
using GRID = uint64_t;

/*
C4_1:

 63 62 61 60 59 58 57 56 15 23 31 39 47 55 63
 55 54 53 52 51 50 49 48 14 22 30 38 46 54 62
 47 46 45 44 43 42 41 40 13 21 29 37 45 53 61
 39 38 37 36 35 34 33 32 12 20 28 36 44 52 60
 31 30 29 28 27 26 25 24 11 19 27 35 43 51 59
 23 22 21 20 19 18 17 16 10 18 26 34 42 50 58
 15 14 13 12 11 10  9  8  9 17 25 33 41 49 57
 56 48 40 32 24 16  8  0  8 16 24 32 40 48 56
 57 49 41 33 25 17  9  8  9 10 11 12 13 14 15
 58 50 42 34 26 18 10 16 17 18 19 20 21 22 23
 59 51 43 35 27 19 11 24 25 26 27 28 29 30 31
 60 52 44 36 28 20 12 32 33 34 35 36 37 38 39
 61 53 45 37 29 21 13 40 41 42 43 44 45 46 47
 62 54 46 38 30 22 14 48 49 50 51 52 53 54 55
 63 55 47 39 31 23 15 56 57 58 59 60 61 62 63

GRID[1...7] is unused

*/

/*
C4_4:

 63 62 61 60 59 58 57 56  7 15 23 31 39 47 55 63
 55 54 53 52 51 50 49 48  6 14 22 30 38 46 54 62
 47 46 45 44 43 42 41 40  5 13 21 29 37 45 53 61
 39 38 37 36 35 34 33 32  4 12 20 28 36 44 52 60
 31 30 29 28 27 26 25 24  3 11 19 27 35 43 51 59
 23 22 21 20 19 18 17 16  2 10 18 26 34 42 50 58
 15 14 13 12 11 10  9  8  1  9 17 25 33 41 49 57
  7  6  5  4  3  2  1  0  0  8 16 24 32 40 48 56
 56 48 40 32 24 16  8  0  0  1  2  3  4  5  6  7
 57 49 41 33 25 17  9  1  8  9 10 11 12 13 14 15
 58 50 42 34 26 18 10  2 16 17 18 19 20 21 22 23
 59 51 43 35 27 19 11  3 24 25 26 27 28 29 30 31
 60 52 44 36 28 20 12  4 32 33 34 35 36 37 38 39
 61 53 45 37 29 21 13  5 40 41 42 43 44 45 46 47
 62 54 46 38 30 22 14  6 48 49 50 51 52 53 54 55
 63 55 47 39 31 23 15  7 56 57 58 59 60 61 62 63
*/

namespace thats_what_the_point_of_the_mask_is {
constexpr GRID enumask = []() {
  GRID ans = 0;
  if constexpr (SYM == C4_1) {
    for (int i = 0; i < N; i++)
      for (int j = 0; j < N; j++)
        if (j == 0 || i != 0)
          ans |= (1ull << (i * 8 + j));
  }
  if constexpr (SYM == C4_4) {
    for (int i = 0; i < N; i++)
      for (int j = 0; j < N; j++)
        ans |= (1ull << (i * 8 + j));
  }
  return ans;
}();
constexpr GRID shellmask = []() {
  GRID ans = 0;
  if constexpr (SYM == C4_1) {
    for (int i = 0; i <= N; i++)
      for (int j = 0; j <= N; j++)
        if ((j == 0 || i != 0) && (i == N || j == N))
          ans |= (1ull << (i * 8 + j));
  }
  if constexpr (SYM == C4_4) {
    for (int i = 0; i <= N; i++)
      for (int j = 0; j <= N; j++)
        if (i == N || j == N)
          ans |= (1ull << (i * 8 + j));
  }
  return ans;
}();

constexpr GRID amask = []() {
  GRID ans = 0;
  if constexpr (SYM == C4_1 || SYM == C4_4) {
    for (int i = 0; i < 8; i++)
      for (int j = 0; j < 8; j++)
        if (i != 7 && j != 7)
          ans |= (1ull << (i * 8 + j));
  }
  return ans;
}();
constexpr GRID bmask = []() {
  GRID ans = 0;
  if constexpr (SYM == C4_1 || SYM == C4_4) {
    for (int i = 0; i < 8; i++)
      for (int j = 0; j < 8; j++)
        if (i != 7)
          ans |= (1ull << (i * 8 + j));
  }
  return ans;
}();
constexpr GRID cmask = []() {
  GRID ans = 0;
  if constexpr (SYM == C4_1 || SYM == C4_4) {
    for (int i = 0; i < 8; i++)
      for (int j = 0; j < 8; j++)
        if (i != 7 && j != 0)
          ans |= (1ull << (i * 8 + j));
  }
  return ans;
}();
constexpr GRID dmask = []() {
  GRID ans = 0;
  if constexpr (SYM == C4_1 || SYM == C4_4) {
    for (int i = 0; i < 8; i++)
      for (int j = 0; j < 8; j++)
        if (j != 7)
          ans |= (1ull << (i * 8 + j));
  }
  return ans;
}();
constexpr GRID fmask = []() {
  GRID ans = 0;
  if constexpr (SYM == C4_1 || SYM == C4_4) {
    for (int i = 0; i < 8; i++)
      for (int j = 0; j < 8; j++)
        if (j != 0)
          ans |= (1ull << (i * 8 + j));
  }
  return ans;
}();
constexpr GRID gmask = []() {
  GRID ans = 0;
  if constexpr (SYM == C4_1 || SYM == C4_4) {
    for (int i = 0; i < 8; i++)
      for (int j = 0; j < 8; j++)
        if (i != 0 && j != 7)
          ans |= (1ull << (i * 8 + j));
  }
  return ans;
}();
constexpr GRID hmask = []() {
  GRID ans = 0;
  if constexpr (SYM == C4_1 || SYM == C4_4) {
    for (int i = 0; i < 8; i++)
      for (int j = 0; j < 8; j++)
        if (i != 0)
          ans |= (1ull << (i * 8 + j));
  }
  return ans;
}();
constexpr GRID imask = []() {
  GRID ans = 0;
  if constexpr (SYM == C4_1 || SYM == C4_4) {
    for (int i = 0; i < 8; i++)
      for (int j = 0; j < 8; j++)
        if (i != 0 && j != 0)
          ans |= (1ull << (i * 8 + j));
  }
  return ans;
}();
} // namespace thats_what_the_point_of_the_mask_is
using namespace thats_what_the_point_of_the_mask_is;

inline GRID _ev(GRID a, GRID b, GRID c, GRID d, GRID e, GRID f, GRID g, GRID h,
                GRID i) {
  GRID v1 = a & b, v2 = a | b, v3 = c & d, v4 = c | d;
  GRID v5 = f & g, v6 = f | g, v7 = h & i, v8 = h | i;
  GRID w1 = v1 & v3, w2 = v1 | v3, w3 = v2 & v4, w4 = v2 | v4;
  GRID w5 = v5 & v7, w6 = v5 | v7, w7 = v6 & v8, w8 = v6 | v8;
  GRID x1 = w2 & w3, x2 = w2 | w3, x3 = w6 & w7, x4 = w6 | w7;
  GRID y1 = w1 | w5, y2 = x1 | x3, y3 = x2 & x4, y4 = x2 | x4, y5 = w4 & w8;
  GRID z1 = y1 | y3, z2 = y2 & y5, z3 = y2 | y5;
  GRID n4 = z1 | z2, n3 = z3 & y4, n2 = z3 | y4;
  return (~n4) & (n3 | (n2 & e));
}
GRID ev(GRID x) {
  static uint64_t m2lut[256];
  for (static uint64_t x = 0; x < 256; x++)
    m2lut[x] = (x & 1) | ((x & 2) << 7) | ((x & 4) << 14) | ((x & 8) << 21) |
               ((x & (1 << 4)) << 28) | ((x & (1 << 5)) << 35) |
               ((x & (1 << 6)) << 42) | ((x & (1 << 7)) << 49);
  if constexpr (SYM == C4_1) {
    GRID r = x & shellmask;
    GRID m1 = (x & 1) | ((x >> 7) & 2) | ((x >> 14) & 4) | ((x >> 21) & 8) |
              ((x >> 28) & (1 << 4)) | ((x >> 35) & (1 << 5)) |
              ((x >> 42) & (1 << 6)) | ((x >> 49) & (1 << 7));
    x |= m1;
    GRID m2 = m2lut[(x >> 8) & 255];
    GRID a = (x & amask) << 9, b = (x & bmask) << 8, c = (x & cmask) << 7,
         d = (x & dmask) << 1, e = x, f = (x & fmask) >> 1,
         g = (x & gmask) >> 7, h = (x & hmask) >> 8, i = (x & imask) >> 9;
    r |= (_ev(a | (m2 << 8) | ((x >> 9) & 1), b | ((x >> 8) & 1),
              c | ((x >> 9) & 1), d | m2, e, f, g | (m2 >> 8), h, i) &
          enumask);
    return r;
  }
  if constexpr (SYM == C4_4) {
    GRID r = x & shellmask;
    GRID m1 = (x & 1) | ((x >> 7) & 2) | ((x >> 14) & 4) | ((x >> 21) & 8) |
              ((x >> 28) & (1 << 4)) | ((x >> 35) & (1 << 5)) |
              ((x >> 42) & (1 << 6)) | ((x >> 49) & (1 << 7));
    GRID m2 = m2lut[x & 255];
    GRID a = (x & amask) << 9, b = (x & bmask) << 8, c = (x & cmask) << 7,
         d = (x & dmask) << 1, e = x, f = (x & fmask) >> 1,
         g = (x & gmask) >> 7, h = (x & hmask) >> 8, i = (x & imask) >> 9;
    r |= (_ev(a | (m1 << 1) | (m2 << 8) | (x & 1), b | m1, c | (m1 >> 1),
              d | m2, e, f, g | (m2 >> 8), h, i) &
          enumask);
    return r;
  }
  return 0;
}

constexpr int MINX = []() {
  if constexpr (SYM == C4_1)
    return 10 - N + 1;
  if constexpr (SYM == C4_4)
    return 10 - N;
}();
constexpr int MAXX = []() {
  if constexpr (SYM == C4_1)
    return 10 + N;
  if constexpr (SYM == C4_4)
    return 10 + N;
}();
constexpr int MINY = []() {
  if constexpr (SYM == C4_1)
    return 10 - N + 1;
  if constexpr (SYM == C4_4)
    return 10 - N;
}();
constexpr int MAXY = []() {
  if constexpr (SYM == C4_1)
    return 10 + N;
  if constexpr (SYM == C4_4)
    return 10 + N;
}();

void fmt(GRID x, int (*g)[20]) {
  if constexpr (SYM == C4_1) {
    for (int i = 0; i < 8; i++)
      for (int j = 0; j < 8; j++)
        g[10 + i][10 + j] = g[10 - j][10 + i] = g[10 - i][10 - j] =
            g[10 + j][10 - i] = !!(x & (1ull << (i * 8 + j)));
  }
  if constexpr (SYM == C4_4) {
    for (int i = 0; i < 8; i++)
      for (int j = 0; j < 8; j++)
        g[10 + i][10 + j] = g[9 - j][10 + i] = g[9 - i][9 - j] =
            g[10 + j][9 - i] = !!(x & (1ull << (i * 8 + j)));
  }
}

std::string output(GRID x) {
  static int g[20][20];
  fmt(x, g);
  std::string res = "x = 0, y = 0, rule = StateInvestigator\n";
  for (int i = MINX - 1; i <= MAXX; i++) {
    for (int j = MINY - 1; j <= MAXY; j++) {
      int c = g[i][j];
      if ((i == MINX - 1) || (j == MINY - 1) || (i == MAXX) || (j == MAXY))
        res += "ED"[c];
      else
        res += ".A"[c];
    }
    res += "$!"[i == 10 + N];
    res += "\n";
  }
  return res;
}

std::string output2(GRID x, bool be = false) {
  static int g[20][20];
  fmt(x, g);
  std::ostringstream s;
  int id = 0;
  for (int i = 0; i < 1; i++) {
    for (int c = 0; c < 6; c++)
      s << "0 ";
    for (int j = MINY; j < MAXY; j++)
      s << "0 ";
    for (int c = 0; c < 6; c++)
      s << "0 ";
    s << "\n";
  }
  for (int i = 0; i < 5; i++) {
    s << "0 ";
    for (int c = 0; c < 5; c++)
      s << "x" << id++ << " ";
    if (be && i == 4)
      for (int j = MINY; j < MAXY; j++)
        s << "* ", id++;
    else
      for (int j = MINY; j < MAXY; j++)
        s << "x" << id++ << " ";
    for (int c = 0; c < 5; c++)
      s << "x" << id++ << " ";
    s << "0\n";
  }
  for (int i = MINX; i < MAXX; i++) {
    s << "0 ";
    for (int c = 0; c < 5; c++) {
      if (be && c == 4)
        s << "* ", id++;
      else
        s << "x" << id++ << " ";
    }
    for (int j = MINY; j < MAXY; j++) {
      int c = g[i][j];
      s << "01"[c] << " ";
    }
    for (int c = 0; c < 5; c++) {
      if (be && c == 0)
        s << "* ", id++;
      else
        s << "x" << id++ << " ";
    }
    s << "0\n";
  }
  for (int i = 0; i < 5; i++) {
    s << "0 ";
    for (int c = 0; c < 5; c++)
      s << "x" << id++ << " ";
    if (be && i == 0)
      for (int j = MINY; j < MAXY; j++)
        s << "* ", id++;
    else
      for (int j = MINY; j < MAXY; j++)
        s << "x" << id++ << " ";
    for (int c = 0; c < 5; c++)
      s << "x" << id++ << " ";
    s << "0\n";
  }
  for (int i = 0; i < 1; i++) {
    for (int c = 0; c < 6; c++)
      s << "0 ";
    for (int j = MINY; j < MAXY; j++)
      s << "0 ";
    for (int c = 0; c < 6; c++)
      s << "0 ";
    s << "\n";
  }
  return s.str();
}