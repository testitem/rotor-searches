#include <iostream>

#include "common.h"

int main() {
  GRID x;
  while (std::cin >> x) {
    std::cout << output(x) << std::endl;
  }
}