T = 8192
X = 262144
F = """executable     = main
request_memory = 128 MB
request_cpus   = 1
request_disk   = 128 MB
log            = search.log
should_transfer_files = yes
arguments      = %d 0 4
out            = out.%d
err            = err.%d
queue 1"""
for i in range(T):
  for j in range(i, X, T):
    open("jobs/J%d.submit" % j, "w").write(F % (j, j, j))
  #   print("JOB J%d jobs/J%d.submit" % (j, j))
  #for j in range(i, X, T):
  #  print("RETRY J%d 100" % (j,))
  #for j in range(i, X - T, T):
  #  print("PARENT J%d CHILD J%d" % (j, j + T))
