import sys
for l in sys.stdin.read().strip().split("\n"):
  l = l.strip()
  print("JOB %s jobs/%s.submit" % (l, l))
  print("RETRY %s 100" % (l,))
