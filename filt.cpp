#include <fstream>
#include <iostream>
#include <set>

#include "common.h"

int main() {
  int p;
  std::cin >> p;
  int n = -1;
  std::cin >> n;
  std::ifstream fin(std::to_string(p) + "-out.txt");
  std::set<GRID> enc;
  GRID g;
  static int t[20][20];
  int cnt = 0;
  while (fin >> g) {
    cnt++;
    GRID x = g, y = g;
    for (int i = 0; i < p; i++) {
      x = ev(x);
      y = std::min(y, x);
    }
    // if (x != g)
    // std::cout << "bees" << std::endl;
    enc.insert(y);
  }
  std::cerr << "read " << cnt << std::endl;
  std::cerr << "distinct " << enc.size() << std::endl;
  int i = 0;
  for (GRID t : enc) {
    i++;
    if (n != -1 && i != n)
      continue;
    std::ofstream bb("bb");

    std::cerr << i << ' ';
    bb << output2(t) << std::endl;
    for (int i = 0; i < p; i++) {
      t = ev(t);
      bb << output2(t, i != p - 1) << std::endl;
    }
    bb.close();
    if (system("python3 lls bb -v 1 -s C4") == 2)
      return 0;
  }
}