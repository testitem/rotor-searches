#include <algorithm>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#include "common.h"

int main(int argc, char *argv[]) {
  int n = -1, sh = 0;
  if (argc > 1)
    n = std::stoi(argv[1]);
  if (argc > 2)
    n += std::stoi(argv[2]);
  if (argc > 3)
    sh = std::stoi(argv[3]);
  GRID pmask = shellmask, cmask = enumask;
  for (int i = 0; sh; i++)
    if (cmask & (1ull << i))
      cmask ^= (1ull << i), pmask ^= (1ull << i), sh--;
  std::vector<GRID> sm;
  for (GRID s = pmask;; s = (s - 1) & pmask) {
    sm.push_back(s);
    if (s == 0)
      break;
  }
  std::reverse(sm.begin(), sm.end());
  clock_t pclo = clock();
  for (int tt = 0; tt < sm.size(); tt++) {
    if (n != -1 && tt != n)
      continue;
    GRID s = sm[tt];
    for (GRID i = cmask;; i = (i - 1) & cmask) {
      GRID x = i | s;
      GRID y = x;
      int p = 0;
      for (int g = 1; g < 20; g++) {
        y = ev(y);
        if (y < x)
          break;
        if (y == x) {
          p = g;
          break;
        }
      }
      if (p >= 5) {
        std::string fname;
        if (n == -1)
          fname = std::to_string(p) + "-out.txt";
        else
          fname = std::to_string(p) + "-" + std::to_string(n) + ".txt";
        std::ofstream re(fname, std::ios_base::app);
        re << x << std::endl;
        re.close();
      }
      if (i == 0)
        break;
    }
    if (n == -1) {
      std::cout << tt + 1 << "/" << sm.size() << " done (c. "
                << (clock() - pclo) / 1e6 << " s)" << std::endl;
    }
    pclo = clock();
  }
}